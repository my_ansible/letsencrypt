#!/usr/bin/env bash

# This script can be used to check if a subjectAltName is in a cert
#
# Usage: $0 /path/to/cert.pem www.example.org

# Check path to a cert on standard input
if [[ ! -f "${1}" ]]; then
  echo "The first parameter should be a path to a cert" 
  exit 1
else
  if [[ -z "${2}" ]]; then
    echo "The second paramater should be a domain name"
    exit 1
  else 
    # Check that the files appears to be a cert
    CERT_CHECK=$(openssl x509 -in "${1}" -text -noout | grep ^Certificate) 
    if [[ "${CERT_CHECK}" != "Certificate:" ]]; then
      echo "${1} doesn't appear to be a cert"
      exit 1
    else
      MATCH=$( openssl x509 -in "${1}" -text -noout | grep "[[:space:]]DNS" | tr -d "DNS:" | tr -d "," | sed 's/ /\n/g' | grep ^"${2}"$ )
      if [[ "${MATCH}" != "${2}" ]]; then
        echo "NOMATCH"
        exit 0
      else
        echo "MATCH"
        exit 0
      fi
    fi
  fi
fi


